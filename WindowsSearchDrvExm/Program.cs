﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.IO;

namespace WindowsSearchDrvExm
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        ///  
        [DllImport("User32.dll")]
        private static extern bool LockWorkStation();
        [DllImport("kernel32.dll")]
        static extern uint GetLastError();
        static  StreamWriter file;

        public static void LockWorkstation()
        {
            bool b = LockWorkStation();
            if (!b)
            {
                uint e =  GetLastError();
                try
                {
                    file.WriteLine(String.Format("error code: 0x{0:X}", e));
                    file.Flush();
                    file.Close();
                }
                catch (IOException ex)
                {
                    file.WriteLine(ex.Message);
                    file.Flush();
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                }
            }
            else
            {
            }
        }

        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }

            if (args[0] == "block")
            {
                file = new StreamWriter(new FileStream(@"Trace.log",
                                System.IO.FileMode.Append, System.IO.FileAccess.Write));
                file.WriteLine("  " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                LockWorkstation();
            }
            //Application.Run();
        }
    }
}
