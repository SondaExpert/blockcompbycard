﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.ServiceProcess;

namespace LocalPCFile
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public class Person
        {
            public String Name;
            public String Fam;
            public String CardNumber;
            public Person(string Name, string Fam, string CardNumber)
             {
                 this.Name = Name;
                 this.Fam = Fam;
                 this.CardNumber = CardNumber;
             }
           

        }
        public string ComAppPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
        public List<Person> PersonList = new List<Person>();
        bool FlagSendVersion = false;

        public string CardNumber = string.Empty; // для записи найденной карты

        private delegate void LineReceivedEvent(string msg);
        public string StringCom = "";

        private void LineReceived(string msg)
        {
            msg = msg.Trim();

            if (msg.Contains("!CARD,0,"))
            {
                String[] substrings = msg.Split(',');
                CardNumber = substrings[2];                             
                return;
            }

            if (msg.Contains("VERSION"))
            {
                if (FlagSendVersion)
                {
                    MessageBox.Show("Устройство обнаружено. Изменения сохранены");
                }
                FlagSendVersion = false;
                SaveComPort();
            }
            else
            {
                if (FlagSendVersion)
                {
                    MessageBox.Show("Устройство не обнаружено по заданному COM порту!");
                }
                FlagSendVersion = false;
            }


        }

        public void SaveToFile()
        {
            BinaryWriter Writer = new BinaryWriter(File.Open(Path.Combine(ComAppPath, "Person.txt"), FileMode.Create));
            try
            {
                foreach (Person per in PersonList)
                {
                    Writer.Write(per.CardNumber);
                    Writer.Write(per.Fam);
                    Writer.Write(per.Name);                   
                }
            }
            finally
            {
                Writer.Close();
            }
        }
        public void ReloadFromFile()
        {
            if (File.Exists(Path.Combine(ComAppPath, "Person.txt")) == false) return;

            var Reader = new BinaryReader(File.OpenRead(Path.Combine(ComAppPath, "Person.txt")));
            try
            {
                while (true)
                {
                    string CardNumber = Reader.ReadString();
                    string Fam = Reader.ReadString();
                    string Name = Reader.ReadString();
                    //MessageBox.Show(CardNumber + " " + Fam);
                    PersonList.Add(new Person(Name, Fam, CardNumber));

                }
            }
            catch (EndOfStreamException ex)
            {
                Reader.Close();
            }
            finally
            {
                Reader.Close();
            }


            ReloadFromList();

            // 
            if (File.Exists(Path.Combine(ComAppPath, "Settings.txt")) == false) return;
            Reader = new BinaryReader(File.OpenRead(Path.Combine(ComAppPath, "Settings.txt")));
            try
            {
               StringCom = Reader.ReadString();
            
            }
            catch (EndOfStreamException ex)
            {
                Reader.Close();
            }
            finally
            {
                Reader.Close();
            }
            comboBoxCom.Text = StringCom;

        }
        public void ReloadFromList()
        {
            dataGridView1.DataSource = null;
            DataTable MyTable = new DataTable();
            MyTable.Columns.Add("Фамилия");
            MyTable.Columns.Add("Имя");
            MyTable.Columns.Add("Номер карты");

            // Заполнение строк таблицы
            
                foreach (Person per in PersonList)
                {
                    MyTable.Rows.Add(new String[] { per.Fam, per.Name, per.CardNumber });
                }
                dataGridView1.DataSource = MyTable;
            

        }


        private void Form1_Load(object sender, EventArgs e)
        {

            using (FormPassw dialog = new FormPassw())
            {
                DialogResult result = dialog.ShowDialog();
                if (result != DialogResult.OK)
                {
                    Application.Exit();
                }

            }

                        dataGridView1.BorderStyle = BorderStyle.None;
            dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            ReloadFromFile();
            // настройка COM порта
            // считываем из файла  номер порта
            if (File.Exists(Path.Combine(ComAppPath, "Settings.txt")) == false)
            {
                // блокируем кнопки добавление, удаления пользователей
                btnAdd.Enabled = false;
                btnDelete.Enabled = false;
                return;
            }
            else
            {
                btnAdd.Enabled = true;
                btnDelete.Enabled = true;
            }

            string COMPort = "";
            var Reader = new BinaryReader(File.OpenRead(Path.Combine(ComAppPath, "Settings.txt")));
            try
            {
                COMPort = Reader.ReadString();
            }

            catch (EndOfStreamException ex)
            {
                Reader.Close();
            }
            finally
            {
                Reader.Close();
            }

            if (COMPort == "")
            {
                MessageBox.Show("Ошибка при открытие файла настроек");
                return;
            }
            // настраиваем COM порт
            try
            {
                serialPort1.PortName = COMPort;
                serialPort1.BaudRate = 9600;
                if (!(serialPort1.IsOpen))
                    serialPort1.Open();
               
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка записи  в порт :: "
                                 + ex.Message, "Error!");

            }
        
        }

        public void SaveComPort()
        {
            BinaryWriter Writer = new BinaryWriter(File.Open(Path.Combine(ComAppPath, "Settings.txt"), FileMode.Create));
             try
             {
                 Writer.Write(comboBoxCom.Text);
             }
             finally
             {
                 Writer.Close();
             }
            // разблокируем кнопки добавления и удаления пользователей.
             btnAdd.Enabled = true;
             btnDelete.Enabled = true;
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            try
            {
                if (!(serialPort1.IsOpen))
                {
                    serialPort1.PortName = comboBoxCom.Text;
                    serialPort1.BaudRate = 9600;

                    serialPort1.Open();

                    serialPort1.WriteLine("!GetVer,0\r\n");
                    FlagSendVersion = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка записи  в порт :: "
                                 + ex.Message, "Error!");
            }
        }

        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            Thread.Sleep(500);
            string msg = serialPort1.ReadLine();
            this.BeginInvoke(new LineReceivedEvent(LineReceived), new object[] { msg });
        }

        private bool isUniqCard(string Card)
        {
            foreach (Person per in PersonList)
            {
                if (per.CardNumber.Trim() == Card.Trim())
                {
                    return false;
                }
            }
            return true;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Приложите карту к считывателю и нажмите ОК");
            
            if (CardNumber != string.Empty)
            {
                MessageBox.Show("Номер карты: "+ CardNumber);

                if (!isUniqCard(CardNumber))
                {
                    MessageBox.Show("Такая карта уже зарегистрирована");
                    return;
                }
                using (AddChangePersonCard dialog = new AddChangePersonCard())
                {
                    DialogResult result = dialog.ShowDialog();
                    if (result == DialogResult.OK)
                    {                       
                        // добавили в список
                        PersonList.Add(new Person(dialog.ReturnValue2, dialog.ReturnValue1, CardNumber));
                        ReloadFromList();
                    }
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (this.dataGridView1.SelectedRows.Count > 0)                
            {                
               // удаляем из списка  
                PersonList.RemoveAll(Person => Person.CardNumber == this.dataGridView1.SelectedRows[0].Cells[2].Value.ToString().Trim());
               
                // удаляем из Grid
                this.dataGridView1.Rows.RemoveAt(this.dataGridView1.SelectedRows[0].Index);
                ReloadFromList();
            }
        }

        private void btnSaveClose_Click(object sender, EventArgs e)
        {
            SaveToFile();
            try
            {
                ServiceController Service = new ServiceController("WindowsSearchDrv");
                Service.Start(); 
            }

            catch  (Exception ex)
            {
                MessageBox.Show("Ошибка запуска службы "+ex.ToString());
            }

            Application.Exit();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

       
    }
}
