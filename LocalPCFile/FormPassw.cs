﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Security.Cryptography;

namespace LocalPCFile
{
    public partial class FormPassw : Form
    {

        public string ComAppPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
        public bool isreg = true;

        public FormPassw()
        {
            InitializeComponent();
        }


        string GetHashString(string s)
        {
            //переводим строку в байт-массим  
            byte[] bytes = Encoding.Unicode.GetBytes(s);

            //создаем объект для получения средст шифрования  
            MD5CryptoServiceProvider CSP =
                new MD5CryptoServiceProvider();

            //вычисляем хеш-представление в байтах  
            byte[] byteHash = CSP.ComputeHash(bytes);

            string hash = string.Empty;

            //формируем одну цельную строку из массива  
            foreach (byte b in byteHash)
                hash += string.Format("{0:x2}", b);

            return hash;
        }


        private void btnOk_Click(object sender, EventArgs e)
        {
            if (isreg && ((textBox1.Text).Trim() != (textBox2.Text).Trim()))
            {
                MessageBox.Show("Пароли не совпадают!");
                return;
            }

            if (!isreg)
            {
                var Reader = new BinaryReader(File.OpenRead(Path.Combine(ComAppPath, "11.dat")));
                string Password = Reader.ReadString();
                if (GetHashString((textBox1.Text).Trim()) == Password)
                {
                    // пароли совпадают
                    this.DialogResult = DialogResult.OK;
                    this.Close();

                }
                else
                {
                    MessageBox.Show("Пароль неверный!");
                    this.DialogResult = DialogResult.Cancel;
                    this.Close();
                }
            }
            else
            {
                BinaryWriter Writer = new BinaryWriter(File.Open(Path.Combine(ComAppPath, "11.dat"), FileMode.Create));
                try
                {
                    Writer.Write(GetHashString((textBox1.Text).Trim()));
                }
                finally
                {
                    Writer.Close();
                }
                MessageBox.Show("Пароль введен!");
                this.DialogResult = DialogResult.OK;
                this.Close();
            }

        }

        private void FormPassw_Load(object sender, EventArgs e)
        {
            if (File.Exists(Path.Combine(ComAppPath, "11.dat")) == true)  // если существует тогда верификация по паролю и повторный ввод пароля не нужен
            {
                label2.Visible = false;
                textBox2.Visible = false;
                textBox2.Enabled = false;
                isreg = false;

            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
