﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace LocalPCFile
{
    public partial class AddChangePersonCard : Form
    {
        public string ReturnValue1 { get; set; }
        public string ReturnValue2 { get; set; }

        public AddChangePersonCard()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if ((textBox1.Text.Trim() == "") ||
               (textBox2.Text.Trim() == ""))
            {
                MessageBox.Show("Поля Фамилия и Имя обязательны для заполнения");
                return;
            }


            this.ReturnValue1 = textBox1.Text;
            this.ReturnValue2 = textBox2.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.ReturnValue1 = "";
            this.ReturnValue2 = "";
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
