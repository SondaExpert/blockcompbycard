﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace LocalPCFile
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>

        [DllImport("User32.dll")]
        private static extern bool LockWorkStation();

        public static void LockWorkstation()
        {
            bool b = LockWorkStation();            
           
        }

        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length != 0)
            {
                if (args[0] == "block")
                {
                    LockWorkstation();
                    System.Diagnostics.Process.GetCurrentProcess().Kill();
                }
            }


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
