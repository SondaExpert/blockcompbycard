﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;


namespace WindowsSearchDrv
{
    public partial class WindowsSearchDrv : ServiceBase
    {
        private StreamWriter file;
        const string cardNumber = "3255855";
        const string comPort = "COM6";
        bool isBlock = true;
        bool FlagSendVersion = false;
        private System.Timers.Timer timer;
        private System.Timers.Timer ConnectToDeviceTick;
        

        //include dll system

        public class Person
        {
            public String Name;
            public String Fam;
            public String CardNumber;
            public Person(string Name, string Fam, string CardNumber)
            {
                this.Name = Name;
                this.Fam = Fam;
                this.CardNumber = CardNumber;
            }


        }

        public string ComAppPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
        public List<Person> PersonList = new List<Person>();
        public string StringCom = "";


        private delegate void LineReceivedEvent(string msg);

        public WindowsSearchDrv()
        {
            InitializeComponent();
            this.CanStop = true;             // службу можно остановить
            this.CanPauseAndContinue = true; // службу можно приостановить и затем продолжить
            this.AutoLog = true;             // служба может вести запись в лог
        }

        private void LockWorkstation()
        {
            try
            {

                String applicationName = @"WindowsSearchDrvExm.exe block";
                // launch the application
                ApplicationLoader.PROCESS_INFORMATION procInfo;
                ApplicationLoader.StartProcessAndBypassUAC(applicationName, out procInfo);

            }
            catch (Exception e)
            {
                WriteLog(e.Message);
            }

            // запускаем программу

        }
        public void SetBlock()
        {
            if (isBlock)
            { WriteLog("Blocked");
              LockWorkstation();
            }
            else
            { WriteLog("UnBlocked"); }

                           
        }

        public bool IsFindCard(string msg)
        {
            foreach (Person per in PersonList)
            {
                if (msg.Contains("!CARD,0," + per.CardNumber.Trim()) && msg.Contains("COMPON"))
                {
                    return true;
                }
            }
            return false;
        } 


        public void ReloadFromFile()
        {

             WriteLog("Shamil");
             if (File.Exists(Path.Combine(ComAppPath, "Person.txt")) == false)
             {
                 WriteLog("Нет файла с сотрдниками");
                 System.Diagnostics.Process.GetCurrentProcess().Kill();
             }

             var Reader = new BinaryReader(File.OpenRead(Path.Combine(ComAppPath, "Person.txt")));
             try
             {
                 while (true)
                 {
                     string CardNumber = Reader.ReadString();
                     string Fam = Reader.ReadString();
                     string Name = Reader.ReadString();
                     //MessageBox.Show(CardNumber + " " + Fam);
                     PersonList.Add(new Person(Name, Fam, CardNumber));

                 }
             }
             catch (EndOfStreamException ex)
             {
                 Reader.Close();
             }
             finally
             {
                 Reader.Close();
             }

                       
             
             if (File.Exists(Path.Combine(ComAppPath, "Settings.txt")) == false)
             {
                     WriteLog("Нет файла конфигурации");
                     System.Diagnostics.Process.GetCurrentProcess().Kill();
                
             }
             Reader = new BinaryReader(File.OpenRead(Path.Combine(ComAppPath, "Settings.txt")));
             try
             {
                 StringCom = Reader.ReadString();

             }
             catch (EndOfStreamException ex)
             {
                 Reader.Close();
             }
             finally
             {
                 Reader.Close();
             }


        }

        private void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            Thread.Sleep(500);
            //WriteLog("serialPort1_DataReceived");
            string msg = serialPort1.ReadLine();
            LineReceived ( msg );
        }

       

        private void LineReceived(string msg)
        {
            //WriteLog("LineReceived");
            timer.Enabled = false;
            ConnectToDeviceTick.Enabled = false;

            WriteLog(msg);
            
            msg = msg.Trim();
            if (IsFindCard(msg))
                WriteLog("Found card");
            else
                WriteLog("Not found card");

            //if (msg.Contains("!CARD,0," + cardNumber) && msg.Contains("COMPON"))
            if (IsFindCard(msg))
            {
                isBlock = false;             
                SetBlock();
                msg = "";
                timer.Enabled = true;
                ConnectToDeviceTick.Enabled = true;
                return;
            }
            else
            {
                if (msg.Contains("!CARD"))  //  команду version обрабытываем дальше не здесь
                {
                    isBlock = true;
                    SetBlock();
                    msg = "";
                    timer.Enabled = true;
                    ConnectToDeviceTick.Enabled = true;
                    return;
                }

            }

            if (msg.Contains("VERSION"))
            {
               SetBlock();

            }
            else
            {
                if (FlagSendVersion) // если отправлял и не получил то блокировать
                    isBlock = true;

                SetBlock();
            }

            FlagSendVersion = false;

            msg = "";
            timer.Enabled = true;
            ConnectToDeviceTick.Enabled = true;
            
        }	

        private void WriteLog(string s)
        {
            if (file != null)
            {
                this.file.WriteLine(s);
                this.file.Flush();               
            } 
        }

        protected override void OnStart(string[] args)
        {
            
            
            try
            {
                file = new StreamWriter(new FileStream("WindowsSearchDrv.log",
                                System.IO.FileMode.Append, System.IO.FileAccess.Write));
                this.file.WriteLine("WindowsServiceDrv стартовал в  " + DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss"));
                this.file.Flush();
            }
            catch (IOException ex)
            {
                this.file.WriteLine(ex.Message);
                this.file.Flush();
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }

            ReloadFromFile();

            this.timer = new System.Timers.Timer(12000);  // 10000 milliseconds = 4 seconds
            this.timer.AutoReset = true;
            this.timer.Elapsed += new System.Timers.ElapsedEventHandler(this.timer1_Tick);
            this.timer.Start();

            this.ConnectToDeviceTick = new System.Timers.Timer(40000);  // 40000 milliseconds = 10 seconds
            this.ConnectToDeviceTick.AutoReset = true;
            this.ConnectToDeviceTick.Elapsed += new System.Timers.ElapsedEventHandler(this.ConnectToDeviceTick_Tick);
            this.ConnectToDeviceTick.Start();

            try
            {
                serialPort1.PortName = StringCom;
                serialPort1.BaudRate = 9600;
                serialPort1.Open();          
                serialPort1.WriteLine("!GetVer,0\r\n");
            }

            catch (Exception ex)
            {
                WriteLog("Ошибка открытия Com порта " + ex.Message);
                System.Diagnostics.Process.GetCurrentProcess().Kill();
            }

            
        }

        protected override void OnStop()
        {
            WriteLog("WindowsServiceDrv остановка");
            if (file != null)
            {             
              
                this.file.Close();
            } 

        }

        protected override void OnPause()
        {
            WriteLog("WindowsServiceDrv пауза");                
            
        }

        protected override void OnContinue()
        {
            WriteLog("WindowsServiceDrv продолжен");
                
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
           // WriteLog("timer1_Tick " + DateTime.Now.ToString());

            if (!FlagSendVersion)  // если порт недоступен или GetVer отправоен, то FlagSendVersion = true блокироваться этим таймером не будет  а только нижним
                 SetBlock();
             if (isBlock)
             {
                 FlagSendVersion = false; // если заблокирован то и отправлят ничего не надо
             }

        }

        private void ConnectToDeviceTick_Tick(object sender, EventArgs e)
        {
           // WriteLog("ConnectToDeviceTick "+ DateTime.Now.ToString());
            SetBlock();  // каждые 40 сек или блокирует или неблокирукт

            try
            {
                if (!(serialPort1.IsOpen))
                    serialPort1.Open();
                serialPort1.WriteLine("!GetVer,0\r\n");
                FlagSendVersion = true;
                
            }
            catch (Exception ex)
            {
                WriteLog("Ошибка записи в порт :: "
                               + ex.Message);
                FlagSendVersion = true; 
                isBlock = true;
            }
           
           
        }

    }
}
